<?php


namespace Application\Services;


use Application\Interfaces\Response;

class PlainResponse implements Response
{
    protected $data;

    public function setData($data)
    {
        $this->data = $data;
    }

    public function send()
    {
        echo $this->data."\n";
    }
}