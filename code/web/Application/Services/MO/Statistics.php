<?php


namespace Application\Services\MO;


use Application\Interfaces\MOStatistics;
use Application\Interfaces\Storage;

class Statistics implements MOStatistics
{
    protected $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    public function getLast15minMOCount()
    {
        /** @var \Application\Repositories\MO $repository */
        $repository = $this->storage->getRepository('Application\\Entities\\MO');
        return $repository->getLast15minMOCount();
    }

    public function getTimeSpanLast10k()
    {
        /** @var \Application\Repositories\MO $repository */
        $repository = $this->storage->getRepository('Application\\Entities\\MO');
        return $repository->getTimeSpanLast10k();
    }
}