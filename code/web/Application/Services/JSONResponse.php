<?php


namespace Application\Services;


class JSONResponse implements \Application\Interfaces\Response
{
    protected $data;

    public function setData($data)
    {
        $this->data = $data;
    }

    public function send()
    {
        echo json_encode($this->data);
    }
}