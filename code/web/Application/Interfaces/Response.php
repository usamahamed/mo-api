<?php


namespace Application\Interfaces;


interface Response
{
    public function setData($data);
    public function send();
}