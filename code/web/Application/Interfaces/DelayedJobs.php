<?php


namespace Application\Interfaces;


use Application\Entities\DelayedJob;

interface DelayedJobs
{
    public function publish(DelayedJob $delayedJob);
    public function subscribe(callable $callback);
    public function getQueueSize();
    public function flushQueue();
}