<?php

namespace Application\Interfaces;


interface Storage
{
    public function getRepository($entityName);
    public function save(Entity $entity);
}