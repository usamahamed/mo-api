<?php

namespace Application\Interfaces;

use DI\Container;

interface DIInjectable
{
    public function setDI(Container $di);
}