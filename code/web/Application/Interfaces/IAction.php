<?php


namespace Application\Interfaces;


interface IAction
{
    public function setRequest(array $request);
    public function run();
}