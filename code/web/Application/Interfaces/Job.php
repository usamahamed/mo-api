<?php


namespace Application\Interfaces;


interface Job
{
    public function process(array $data);
}