<?php


namespace Application\Interfaces;


interface Entity
{
    public function getNamespace();
    public function fromArray(array $data);
    public function toArray();
}