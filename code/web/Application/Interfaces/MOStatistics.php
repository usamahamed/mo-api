<?php


namespace Application\Interfaces;


interface MOStatistics
{
    public function getLast15minMOCount();
    public function getTimeSpanLast10k();
}