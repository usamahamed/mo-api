<?php


namespace Application\Interfaces;


use Application\Entities\MORequest;

interface MO
{
    public function handle(MORequest $MORequest);
    public function process(MORequest $MORequest);
    public function getNotProcessedCount();
    public function flushNotProcessed();
}