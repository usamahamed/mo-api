<?php


namespace Application;

use Application\Interfaces\DIInjectable;
use DI\Container;

class BaseFactory
{
    /** @var Container  */
    protected static $di;

    public static function setDIContainer($di)
    {
        self::$di = $di;
    }

    public static function getInstance($className)
    {
        $obj = new $className;

        if($obj instanceof DIInjectable)
            $obj->setDI(self::$di);

        self::$di->injectOn($obj);

        return $obj;
    }
}