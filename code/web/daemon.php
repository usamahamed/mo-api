<?php

require __DIR__."/vendor/autoload.php";
require __DIR__."/bootstrap.php";

/** @var \Application\Daemon $app */
$app = \Application\BaseFactory::getInstance(\Application\Daemon::class);
$app->run();
